import crypto from 'crypto'
export const random = () => crypto.randomBytes(128).toString('base64')


const SECRET = 'EMNA-KAMMOUN-API'
export const authentication= (salt : string , password: string) => {
    //createHmac: créer un nouvel objet HMAC avec l'algorithme SHA-256
    //'sha256' indique l'algorithme de hachage à utiliser
    //[salt, password].join('/') est la donnée brute que nous allons hacher. Dans ce cas, il combine la chaîne salt et la chaîne password en les joignant avec le caractère '/'
    return crypto.createHmac('sha256' , [salt ,password].join('/')).update(SECRET).digest('hex')
}

//La fonction retourne le résultat du hachage sous forme d'objet HMA